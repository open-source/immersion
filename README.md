# WIDGET IMMERSION

**La Bonne Immersion** cible des entreprises à contacter pour faire une demande d'immersion professionnelle car elles ont des perspectives d'embauche élevées pour un métier donné. Le ciblage est obtenu par l'analyse de millions de recrutements sur toutes les entreprises françaises et est réalisé par **La Bonne Boîte**.
 
Le widget **La Bonne Immersion** permet de récupérer la liste des entreprises  pour un métier (code ROME) et pour une localisation donnée. 
Il permet d’intégrer sur votre site web un module de recherche d'entreprises pouvant accueillir des stagiaires en immersion. Il est conseillé d'accompagner ce module de recherche d'une présentation de l'immersion professionnelle (par exemple [l'immersion professionnelle mode d'emploi](https://youtu.be/2Jy_K6o7lQA) ) et de conseils pour aider les utilisateurs à faire une demande.
Le widget propose en téléchargement le formulaire CERFA permettant de contracter une immersion entre le bénéficiaire, l'entreprise d'accueil et le prescripteur et une plaquette de conseils pour aider les utilisateurs à faire une demande.

Il existe en deux formats (`horizontal`, `vertical`) et supporte tous les navigateurs modernes (Firefox 32+, Chrome 37+, Opera 30+, Safari OS X, Internet Explorer 11+).

Vous pouvez consulter des exemples d'intégration du widget à l'url suivante [https://labonneformation.pole-emploi.fr/demo-widget](https://labonneformation.pole-emploi.fr/demo-widget)


## Options

En plus de l'affichage des entreprises, il est possible d'activer des options dans le widget :

### data-lieu

L'option `data-lieu` permet de pré-remplir le formulaire avec un lieu de recherche, afin d'afficher directement une liste d'entreprises.

Si cette option est renseignée, l'option data-metier doit aussi l'être.

L'utilisateur pourra toujours modifier le métier et le lieu de recherche dans le formulaire.

Le format utilisé pour le lieu est le nom du lieu (ville, département, région) en ASCII, les espaces remplacés par les tirets ("slug"). Il est suivi du code de département pour les villes et les départements.

Ex: `marseilles-les-aubigny-18` pour la ville Marseilles lès Aubigny (18), `loire-atlantique-44` le département Loire-Atlantique (44) ou `pays-de-la-loire` pour la région Pays de la Loire.


### data-metier

L'option `data-metier` permet de pré-remplir le formulaire avec un métier, afin d'afficher directement une liste d'entreprises.

Si cette option est renseignée, l'option data-lieu doit aussi l'être.

L'utilisateur peut toujours modifier le métier et le lieu de recherche dans le formulaire. 

Le format utilisé pour le métier est le libellé du [ROME](https://www.pole-emploi.fr/candidat/le-code-rome-et-les-fiches-metiers-@/article.jspz?id=60702), en minuscule et en ASCII, l'espace remplacé par des tirets ("slug").

Ex : `coiffure-et-maquillage-spectacle` pour [Coiffure et maquillage spectacle (ROME : L1501)]([https://candidat.pole-emploi.fr/marche-du-travail/fichemetierrome?codeRome=L1501) ou `assistance-aupres-adultes` pour [Assistance auprès d'adultes (ROME : K1302)](https://candidat.pole-emploi.fr/marche-du-travail/fichemetierrome?codeRome=K1302).


### data-format

L'option `data-format` permet de choisir entre le format `horizontal` et `vertical` pour le widget.

Si elle n'est pas renseignée, c'est le format `horizontal` qui est utilisé.


## Configuration du widget

Insérez la balise suivante de préférence dans la balise `<head>` de votre document HTML.

```html
<script src="https://labonneformation.pole-emploi.fr/js/inc/widget_immersion_loader.js"></script>
```

Positionner le code suivant à l'emplacement où vous souhaitez afficher le widget dans votre page HTML:

```html
<div class="immersion-widget" data-metier="" data-lieu="" data-format=""></div>"
```

### Paramétrage des données

| Paramètre     | Valeurs                                                   |                             |
| ------------- | -------------                                             | -------------               |
| metier        | ex: coiffure                                              | Optionnel                   |
| lieu          | ex: nantes-44                                             | Optionnel                   |
| format        | horizontal ou vertical                                    | Optionnel                   |


### Exemples


Affichage au format horizontal

```html
<div class="immersion-widget" data-metier="" data-lieu="" data-format="horizontal"></div>

```

Affichage au format vertical

```html
<div class="immersion-widget" data-metier="" data-lieu="" data-format="vertical"></div>

```
 
Affichage avec un métier et un lieu pré-remplis

```html
<div class="immersion-widget" data-metier="coiffure" data-lieu="nantes-44" data-format=""></div>
```

## Rendu

### Formulaire

![](widget-immersion-formulaire.png)

### Liste d'entreprises

![](widget-immersion-liste-entreprises.png)


